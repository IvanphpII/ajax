var url = "http://netology-hj-ajax.herokuapp.com/homework/login_json";
var btnSI = document.querySelector('.signinn');
var btnSO = document.querySelector('.signout');
var preLoader = document.querySelector('.preloader');
var fullname = document.querySelector('.fullname');
var country = document.querySelector('.country');
var fullname = document.querySelector('.fullname');
var hobbies = document.querySelector('.hobbies');
var error = document.querySelector('.error');
var avatar = document.querySelector('.img');
var form = document.querySelector('.form');
var password = document.querySelector('.password');
var email = document.querySelector('.email');
var labels = document.querySelectorAll('label');


function AddHidden(arr) {
		for (var i = 0; i < arr.length; i++) {
		arr[i].classList.add('hidden');
	}
}
function RemoveHidden(arr) {
		for (var i = 0; i < arr.length; i++) {
		arr[i].classList.remove('hidden');
	}
}

form.addEventListener('submit', function(e){
	e.preventDefault();
	if (window.XMLHttpRequest) {
	var xhr = new XMLHttpRequest();
	}
	var inp_email = e.target.email.value;
	var inp_password = e.target.password.value;
	var params = 'email=' + encodeURIComponent(inp_email) + "&" + 'password=' + encodeURIComponent(inp_password);
	xhr.open("POST", "http://netology-hj-ajax.herokuapp.com/homework/login_json");
	
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

	xhr.addEventListener('load', function(){
		
		if (xhr.status >= 200 && xhr.status < 300) {

			var user = JSON.parse(xhr.responseText);
			console.log(user)
			// RemoveHidden(requestUserProfile)
			RemoveHidden([fullname,avatar,country,hobbies,btnSO])

			fullname.innerText = user.name + " " + user.lastname;
			avatar.src = user.userpic;
			country.innerText = user.country;
			hobbies.innerText = user.hobbies;

		} else {
			error.innerText = "Произошла ошибка " + xhr.status + " " + xhr.statusText;
			setTimeout(function(){error.classList.add("hidden")}, 15000)
		}

	});

	xhr.addEventListener('loadstart', function(){
		preLoader.classList.remove('hidden');
		AddHidden(labels);
		AddHidden([btnSI]);
	});
	xhr.addEventListener('loadend', function(){
		preLoader.classList.add('hidden');
		RemoveHidden(labels);
		RemoveHidden([btnSI]);

		if (xhr.status >= 200 && xhr.status < 300) {
			AddHidden(labels);
			AddHidden([btnSI]);
		}

	});


	xhr.send(params);


})
btnSO.addEventListener('click', function() {
	AddHidden([fullname,avatar,country,hobbies,btnSO]);
	RemoveHidden(labels);
	RemoveHidden([btnSI]);
			})